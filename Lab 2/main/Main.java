package main;

import dataTypes.*;

public class Main {

    public static void main(String[] args) {
        TimeHistory timeTest = new TimeHistory<>("Device 1", "This is device 1", 0, 2, "kW", 100.0, new Double[]{1.0,2.0,10.0,0.0,3.0}, 1.0);
        Spectrum spectrumTest = new Spectrum<Double>("Device 2", "Description...", System.currentTimeMillis(), 1, "V", 10.0, Spectrum.Scaling.LOGARYTHMIC);
        Alarm alarmTest = new Alarm<>("Device 3", 3, 0.7, Alarm.Direction.RISE);
        System.out.println(timeTest);
        System.out.println();
        System.out.println(spectrumTest);
        System.out.println();
        alarmTest.setDirection(Alarm.Direction.FALL);
        System.out.println(alarmTest);
    }
}