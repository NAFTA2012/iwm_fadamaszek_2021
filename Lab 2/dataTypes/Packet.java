package dataTypes;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Packet implements Serializable {
    protected String device;
    protected String description;
    protected long date;

    public Packet(){
        device = "Unnamed";
        description = "No_description";
        date = System.currentTimeMillis();
    }

    public Packet(String device, String description, long date){
        this.device = device;
        this.description = description;
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public String getDevice() {
        return device;
    }

    public String getDateString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
        Date dateHolder = new Date(date);
        return formatter.format(dateHolder);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String toString(){
        return "\nDevice name:\t" + device + "\nDescription:\t" + description + "\nDate:\t\t\t" + new Date(date);
    }
}