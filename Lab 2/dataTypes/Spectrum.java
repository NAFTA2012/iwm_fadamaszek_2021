package dataTypes;

public class Spectrum<T> extends Sequence<T> {
    public enum Scaling{
        LOGARYTHMIC(true),
        LINEAR(false);

        boolean type;
        Scaling(boolean type){
            this.type = type;
        }

        public String toString(){
            return super.toString().toLowerCase();
        }
    }
    private Scaling scaling;

    public Spectrum(){
        super();
        this.scaling = Scaling.LOGARYTHMIC;
    }

    public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution, Scaling scaling){
        super(device, description, date, channelNr, unit, resolution);
        this.scaling = scaling;
    }

    public Spectrum(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer, Scaling scaling){
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.scaling = scaling;
    }

    public void setScaling(Scaling scaling) {
        this.scaling = scaling;
    }

    public String toString(){
        return super.toString() + "\nScaling:\t" + scaling.toString().toLowerCase();
    }
}