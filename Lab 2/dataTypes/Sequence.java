package dataTypes;

public abstract class Sequence<T> extends Packet {
    protected int channelNr;
    protected String unit;
    protected double resolution;
    protected T[] buffer;

    public Sequence(){
        super();
        this.channelNr = -1;
        this.unit = "U";
        this.resolution = 1.0;
        this.buffer = null;
    }

    public Sequence(String device, String description, long date, int channelNr, String unit, double resolution){
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = null;
    }

    public Sequence(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer){
        super(device, description, date);
        this.channelNr = channelNr;
        this.unit = unit;
        this.resolution = resolution;
        this.buffer = buffer;
    }

    public void setChannelNr(int channelNr) {
        this.channelNr = channelNr;
    }

    public void setBuffer(T[] buffer) {
        this.buffer = buffer;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String toString(){
        String result = super.toString() + "\nChannel number:\t" + channelNr + "\nUnit:\t\t\t" + unit + "\nResolution:\t\t" + resolution + "\nBuffer:";
        if(buffer != null){
            for(int i = 0; i < buffer.length; i++){
                result = result + "\n\t" + i + ".\t" + buffer[i];
            }
        }else{
            result = result + "\n\tnull";
        }
        return result;
    }
}