package dataTypes;

public class Alarm<T> extends Packet {
    public enum Direction{
        RISE(1),
        FALL(-1),
        ANY(0);

        int direction;

        Direction(int direction){
            this.direction = direction;
        }

        public String toString(){
            return super.toString().toLowerCase();
        }
    }

    private int channelNr;
    private T threshold;
    private Direction direction;

    public Alarm(){
        super();
        this.channelNr = -1;
        this.direction = Direction.RISE;
        this.threshold = null;
    }

    public Alarm(String device, int channelNr, T threshold, Direction direction){
        super(device, "No description", System.currentTimeMillis());
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    public Alarm(String device, String description, long date, int channelNr, T threshold, Direction direction){
        super(device, description, date);
        this.channelNr = channelNr;
        this.threshold = threshold;
        this.direction = direction;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public void setChannelNr(int channelNr) {
        this.channelNr = channelNr;
    }

    public void setThreshold(T threshold){
        this.threshold = threshold;
    }

    public String toString(){
        return super.toString() + "\nChannel number:\t" + channelNr + "\nThreshold:\t\t" + threshold + "\nDirection:\t\t" + direction;
    }
}