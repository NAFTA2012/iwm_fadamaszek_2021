package dataTypes;

public class TimeHistory<T> extends Sequence<T> {
    private double sensitivity;

    public TimeHistory(){
        super();
        this.sensitivity = 1.0;
    }

    public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution, double sensitivity){
        super(device, description, date, channelNr, unit, resolution);
        this.sensitivity = sensitivity;
    }

    public TimeHistory(String device, String description, long date, int channelNr, String unit, double resolution, T[] buffer, double sensitivity){
        super(device, description, date, channelNr, unit, resolution, buffer);
        this.sensitivity = sensitivity;
    }

    public TimeHistory(String device, int channelNr, String unit, double resolution, T[] buffer, double sensitivity){
        super(device, "No_description", System.currentTimeMillis(), channelNr, unit, resolution, buffer);
        this.sensitivity = sensitivity;
    }

    public void setSensitivity(double sensitivity) {
        this.sensitivity = sensitivity;
    }

    public String toString(){
        return super.toString() + "\nSensitivity:\t" + sensitivity;
    }
}