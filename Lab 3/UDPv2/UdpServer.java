package UDPv2;
import dataTypes.Packet;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpServer {
  private enum serverState{
    WAIT,
    RECEIVING,
    SENDING
  }

    public static void main(String[] args) {
    serverState state = serverState.WAIT;
    DatagramSocket aSocket = null;
    String fileName = "";
    Packet packet = null;
    DatagramPacket reply = null;

      try {
        aSocket = new DatagramSocket(9876);
        byte[] buffer;
        byte[] data = new byte[1024];
        DatagramPacket request = null;
        Packet read = null;
        while(true) {

          if(state != serverState.SENDING) {
            buffer = new byte[1024];
            request = new DatagramPacket(buffer, buffer.length);
            switch (state){
              case WAIT:
                System.out.println("\nWaiting for request...");
                break;
              case RECEIVING:
                System.out.println("\nWaiting for data...");
                break;
            }
            aSocket.receive(request);
            String t = new String(request.getData(), 0, request.getLength());
            System.out.println("Received: " + Tools.deserialize(buffer) + "\n");
            reply = new DatagramPacket(request.getData(), request.getLength(),request.getAddress(), request.getPort());
            aSocket.send(reply);

            read = (Packet) Tools.deserialize(buffer);
          }

          switch (state){
            case WAIT:
              if(read instanceof UDPv2.Request){
                switch( ((Request) read).type ){
                  case READ:
                    state = serverState.SENDING;
                    break;
                  case WRITE:
                    state = serverState.RECEIVING;
                    break;
                }
              } else {
                System.out.println("Object of class Request is expected");
                state = serverState.WAIT;
              }
              break;
            case SENDING:
              fileName = read.getDevice() + "-" + read.getDescription() + "-" + read.getDateString() + ".txt";
              File redability = new File(fileName);

              if (redability.isFile() && redability.canRead()){
                InputStream is = new FileInputStream(fileName);
                data = is.readAllBytes();
                Packet loaded = (Packet) Tools.deserialize(data);
                data = Tools.serialize(loaded);
                reply = new DatagramPacket(data, data.length, request.getAddress(), request.getPort());
                aSocket.send(reply);

                System.out.println("Requested file send");
              } else {
                packet = new Request(Request.requestType.NOFILE,0,read.getDevice(), read.getDescription(), read.getDateString());
                data = Tools.serialize(packet);
                reply = new DatagramPacket(data, data.length, request.getAddress(), request.getPort());
                aSocket.send(reply);
                System.out.println("No file of given name. Client informed");
              }
              state = serverState.WAIT;
              break;
            case RECEIVING:
              if(!(read instanceof Request)){
                fileName = read.getDevice() + "-" + read.getDescription() + "-" + read.getDateString() + ".txt";
                OutputStream os = new FileOutputStream(fileName);

                os.write(Tools.serialize(read));
                os.close();
                System.out.println("File successfully saved");
              } else {
                System.out.println("Invalid packet object. Object of class other than Request is expected. Receiving aborted");
                state = serverState.WAIT;
              }
              break;
            default:
              System.out.println("Unexpected behaviour, reverting state to wait");
              state = serverState.WAIT;
          }

        }
      } catch (SocketException ex) {
        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
        Logger.getLogger(UdpServer.class.getName()).log(Level.SEVERE, null, ex);
      } catch (Exception ex){
        Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
      }   finally {
				aSocket.close();
			}
      
    }
}