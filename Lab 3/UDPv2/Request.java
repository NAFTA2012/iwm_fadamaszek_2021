package UDPv2;

import dataTypes.Packet;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Request extends Packet implements Serializable {
    public enum requestType{
        NOFILE(-1),
        READ(0),
        WRITE(1);

        int rq;

        requestType(int type){
            this.rq = type;
        }

        public String toString(){
            return super.toString().toLowerCase();
        }
    }

    public requestType type;
    public int value;

    public Request()
    {
        super();
        this.type = requestType.WRITE;
        this.value= 0;
    }

    public Request(requestType type, int value)
    {
        super();
        this.type = type;
        this.value = value;
    }

    public Request(requestType type, int value, String device, String description, String dateString) throws ParseException {
        super(device, description, new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss").parse(dateString).getTime());
        this.type = type;
        this.value = value;
    }

    public requestType getType() {
        return type;
    }

    public String toString() {
        return "Request:" + "\n\ttype\t" + type + "\n\tvalue\t" + value + super.toString();
    }
}
