package UDPv2;

import dataTypes.Alarm;
import dataTypes.Packet;
import dataTypes.Spectrum;
import dataTypes.TimeHistory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UdpClient {
	public enum clientState{
		EXIT(0),
		IDLE(1),
		SEND(2),
		REQUEST(3),
		EXECUTE(4);

		int type;

		clientState(int type){
			this.type = type;
		}
	}

	public enum dataType {
		NONE(0),
		TIMEHISTORY(1),
		SPECTRUM(2),
		ALARM(3);

		int type;

		dataType(int type){
			this.type = type;
		}
	}

	public static void main(String[] args) {
		clientState state = clientState.IDLE;
		dataType type = dataType.NONE;
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		Packet packet1 = new Request();
		Packet packet2 = new Request();

		try {
			byte[] data;
			DatagramPacket dataPacket;
			byte[] buffer = new byte[1024];
			// args contain server hostname
			if (args.length < 1) {
				System.out.println("Usage: UDPClient <server host name>");
				System.exit(-1);
			}

			InetAddress aHost = InetAddress.getByName(args[0]);//InetAddress.getLocalHost();
			int serverPort = 9876;
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			String line = "";

			while (state != clientState.EXIT) {
				String reqName = "";
				String reqDesc = "";
				String reqDate = "";

				state = showInformation(state, type);

				if (scanner.hasNextLine())
					line = scanner.nextLine();

				switch (state){
					case EXECUTE:
						break;
					case IDLE:
						type = dataType.NONE;
						switch (line.toLowerCase()){
							case "x":
								System.out.println("Exitting");
								state = clientState.EXIT;
								break;
							case "s":
								System.out.println("Sending selected");
								state = clientState.SEND;
								break;
							case "r":
								System.out.println("Requesting selected");
								state = clientState.REQUEST;
								break;
							default:
								System.out.println("Unknown command");
								state = clientState.IDLE;
						}
						break;
					case SEND:
						packet1 = new Request();
						switch (line.toLowerCase()){
							case "t":
								System.out.println("Sending demo time history packet");
								packet2 = new TimeHistory<>();
								state = clientState.EXECUTE;
								type = dataType.TIMEHISTORY;
								break;
							case "s":
								System.out.println("Sending demo spectrum packet");
								packet2 = new Spectrum<>();
								state = clientState.EXECUTE;
								type = dataType.SPECTRUM;
								break;
							case "a":
								System.out.println("Sending demo alarm packet");
								packet2 = new Alarm<>();
								state = clientState.EXECUTE;
								type = dataType.ALARM;
								break;
							default:
								System.out.println("Sending cancelled");
								state = clientState.IDLE;
						}
						break;
					case REQUEST:
						reqName = line;
						if (scanner.hasNextLine())
							reqDesc = scanner.nextLine();
						if (scanner.hasNextLine())
							reqDate = scanner.nextLine();
						packet1 = new Request(Request.requestType.READ,0,reqName,reqDesc,reqDate);
						type = dataType.NONE;
						state = clientState.EXECUTE;
						break;
				}

				if (state == clientState.EXECUTE){
					data = Tools.serialize(packet1);
					dataPacket = new DatagramPacket(data, data.length, aHost, serverPort);
					aSocket.send(dataPacket);

					if (type != dataType.NONE) {
						data = Tools.serialize(packet2);
						dataPacket = new DatagramPacket(data, data.length, aHost, serverPort);
						aSocket.send(dataPacket);
						type = dataType.NONE;
					} else {
						DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
						aSocket.receive(reply);

						Packet read = (Packet) Tools.deserialize(buffer);
						System.out.println("Data received from server");

						if (!(read instanceof Request)){
							String fileName = "client_received-" + read.getDevice() + "-" + read.getDescription() + "-" + read.getDateString() + ".txt";
							OutputStream os = new FileOutputStream(fileName);

							os.write(Tools.serialize(read));
							os.close();
							System.out.println("File successfully saved");
						} else {
							System.out.println("Requested file does not exist");
						}
					}

					state = clientState.IDLE;
				}
			}
		} catch (SocketException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception ex){
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			scanner.close();
		}
	}

	private static clientState showInformation(clientState state, dataType type){
		if(type == dataType.NONE) {

			switch (state) {
				case IDLE:
					System.out.print("R - request data, S - send data, X - exit: ");
					break;
				case SEND:
					System.out.print("T - time history, S - spectrum, A - alarm, C - cancel: ");
					break;
				case REQUEST:
					System.out.println("request format: deviceName [Enter] description [Enter] yyyy_MM_dd-HH_mm_ss [Enter]");
					break;
				default:
					System.out.println("Unexpected behaviour, reverting state to idle.");
					state = clientState.IDLE;
			}

		} else {

			switch (state) {
				case EXECUTE:
					System.out.println("Sending to server");
					break;
				case SEND:
					System.out.println("Sending demo packet: ");
					break;
				case REQUEST:

					break;
				default:
					System.out.println("Unexpected behaviour, reverting state to idle.");
					state = clientState.IDLE;
			}

		}
		return state;
	}
}