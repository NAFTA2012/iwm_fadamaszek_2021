package UDPv1;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import dataTypes.*;

public class UdpClient {
	public static void main(String[] args) {
		DatagramSocket aSocket = null;
		//Scanner scanner = null;
		Packet packet = new TimeHistory<>("Sensor 1", 1, "V", 0.1, new Double[]{0.0,0.5,0.6,0.8,1.2}, 2.0);

		try {
			byte[] data = Tools.serialize(packet);
			// args contain server hostname
			/*if (args.length < 1) {
				System.out.println("Usage: UDPClient <server host name>");
				System.exit(-1);
			}*/
			byte[] buffer = new byte[1024];
			InetAddress aHost = InetAddress.getLocalHost();//getByName(args[0]);
			int serverPort = 9876;
			aSocket = new DatagramSocket();
			//scanner = new Scanner(System.in);
			//String line = "";
			while (true) {
				DatagramPacket dataPacket = new DatagramPacket(data, data.length, aHost, serverPort);
				aSocket.send(dataPacket);

				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);

				Packet read = (Packet) Tools.deserialize(buffer);
				System.out.println("Data received from server:\n" + read);

				/*System.out.println("Enter your message: ");
				if (scanner.hasNextLine())
					line = scanner.nextLine();
				DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
				aSocket.send(request);
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);
				System.out.println("Reply: " + new String(reply.getData(), 0, reply.getLength()));*/
			}
		} catch (SocketException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception ex){
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			//scanner.close();
		}
	}
}