package RmiClient;

import RmiInterface.Interfaces;
import dataTypes.Spectrum;
import dataTypes.TimeHistory;

import java.rmi.registry.Registry;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class Client {

	public static void main(String[] args) {
		
		if (args.length < 1) {
			System.out.println(" Usage : RmiClient <server host name >");
			System.exit(-1);
		}

		TimeHistory data = new TimeHistory<>();
		Interfaces.type dataType = Interfaces.type.TIMEHISTORY;
		//Spectrum data = new Spectrum<>();
		//Interfaces.type dataType = Interfaces.type.SPECTRUM;
		String searchInfo = "Unnamed";

		try {
			Registry reg = LocateRegistry.getRegistry(9876);
			Interfaces serverInt = (Interfaces) reg.lookup("RmiServer");
			boolean daneRegister = serverInt.register(data);
			System.out.println("Client started");
			if (daneRegister) {
				System.out.println("File saved to server");
			}

			int index = 1;
			String dataServer = serverInt.getRegister(dataType, searchInfo);
			System.out.println(dataServer);
			String response = serverInt.saveObject(dataType);
			System.out.println(response);
			Object receivedObject = serverInt.getObject(dataType, index);
			System.out.println("Received file:\n"+ index + ". " + receivedObject.toString());

		} catch(RemoteException e) {
			e.printStackTrace();
		} catch(NotBoundException e) {
			e.printStackTrace();
		}
	}
}