package RmiServer;

import RmiInterface.Interfaces;
import dataTypes.*;
import java.util.ArrayList;
import java.util.List;
import java.rmi.registry.Registry;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class Server implements Interfaces {

	private final List<TimeHistory> TimeHistoryList = new ArrayList<>();
	private final List<Spectrum> SpectrumList = new ArrayList<>();

	public static void main(String[] args) throws AlreadyBoundException {
		Registry reg = null;

		try {
			reg = LocateRegistry.createRegistry(9876);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		try {
			Server server = new Server();
			Interfaces serverInt = (Interfaces) UnicastRemoteObject.exportObject(server, 0);
			assert reg != null;
			reg.bind("RmiServer", serverInt);
			System.out.println("RmiServer started");
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}
	
	public Object getObject(Interfaces.type dataType, int listNumber) {

		Object output = null;
		
		switch (dataType) {
			case TIMEHISTORY:
				if(listNumber <= TimeHistoryList.size() && listNumber > 0)
					output = TimeHistoryList.get(listNumber-1);
				else
					System.out.println("Incorrect Time History list number");
			break;
			case SPECTRUM:
				if(listNumber <= SpectrumList.size() && listNumber > 0)
					output = SpectrumList.get(listNumber-1);
				else
					System.out.println("Incorrect Spectrum list number");
			break;
		}

		return output;
	}

	public String saveObject(Interfaces.type dataType) {

		switch (dataType) {
			case TIMEHISTORY:
				TimeHistory<?> timeHist = TimeHistoryList.get(TimeHistoryList.size() - 1);
				try {

					FileOutputStream fileOut = new FileOutputStream(timeHist.getDevice() + "-" + timeHist.getDescription() + "-" + timeHist.getDateString() + ".thi");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(timeHist);
					objectOut.close();
					System.out.println("File saved:\n" + timeHist);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;
			case SPECTRUM:
				Spectrum<?> spectr = SpectrumList.get(SpectrumList.size() - 1);
				try {
					FileOutputStream fileOut = new FileOutputStream(spectr.getDevice() + "-" + spectr.getDescription() + "-" + spectr.getDateString() + ".spc");
					ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
					objectOut.writeObject(spectr);
					objectOut.close();
					System.out.println("File saved:\n" + spectr);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;
		}

		return "Saving complete";
	}

	public boolean register(Object object) {
		
		System.out.println("Saving file");

		if(object instanceof TimeHistory)
			TimeHistoryList.add((TimeHistory<?>) object);
		else if(object instanceof Spectrum)
			SpectrumList.add((Spectrum<?>) object);
		else {
			System.out.println("Error incorrect class");
			return false;
		}
		
		return true;
	}

	public String getRegister(Interfaces.type dataType, String search) {

		String output = null;

		if(dataType == Interfaces.type.TIMEHISTORY) {
			int i = 1;
			output = "Time History List:";

			for(int j = 1; j <= TimeHistoryList.size(); j++) {

				if(TimeHistoryList.get(i-1).toString().matches("(.*)" + search + "(.*)"))
						output = output + "\n" + i + ". " + TimeHistoryList.get(i++-1);
			}

		} else {
			int i = 1;
			output = "Spectrum List:";

			for(int j = 1; j <= SpectrumList.size(); j++) {

				if(SpectrumList.get(i-1).toString().matches("(.*)" + search + "(.*)"))
					output = output + "\n" + i + ". " + SpectrumList.get(i++-1);
			}

		}
		return output;
	}

}