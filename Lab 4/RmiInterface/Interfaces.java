package RmiInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Interfaces extends Remote {
	public enum type{
		TIMEHISTORY,
		SPECTRUM;
	}

	boolean register(Object obj) throws RemoteException;

	String getRegister(type dataType, String y) throws RemoteException;

	String saveObject(type dataType) throws RemoteException;

	Object getObject(type dataType, int listNumber) throws RemoteException;
}
