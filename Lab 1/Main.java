public class Main {

    public static void ACconverterTest()
    {
        ACconverter s1, s2;
        short[] input = new short[64];
        short[] input2 = new short[16];

        for(short i = 0; i < input.length; i++)
        {
            input[i] = i;
        }
        for(short i = 0; i < input2.length; i++)
        {
            input2[i] = i;
        }

        s1 = new ACconverter(0, 16, 0.2, 2.0, 10.0, input);
        s2 = new ACconverter(1, 8, 0.0, 5.0, 20.0, input2, "V", "Voltage sensor 1");
        System.out.println("Mean: " + s1.mean());
        System.out.println("RMS: " + s1.rms());
        System.out.println("Val1(16): " + s1.value(16));
        System.out.println("Val1(128): " + s1.value(128));
        System.out.println("Val1(63): " + s1.value(63));
        System.out.println("Val1(-1): " + s1.value(-1) + "\n");
        System.out.println(s2);
    }

    public static void main(String[] args) {
        ACconverterTest();//Lab 1 - testing
    }
}
