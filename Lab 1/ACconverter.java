public class ACconverter {
    public int channelNr;
    public String channelName;
    public int bits;
    public double rangeMin, rangeMax;
    public double sensitivity;
    public String unit;
    public double samplingFreq;
    public int length;
    public short[] samples;

    public ACconverter(int channelNr, int bits, double rangeMin, double rangeMax, double samplingFreq, short[] samples, String unit, String channelName)
    {
        super();
        this.channelNr = channelNr;
        this.bits = bits;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.sensitivity = (rangeMax-rangeMin)/(Math.pow(2,bits)-1);
        this.samplingFreq = samplingFreq;
        this.samples = samples;
        this.channelName = channelName;
        this.unit = unit;
        this.length = this.samples.length;
    }

    public ACconverter(int channelNr, int bits, double rangeMin, double rangeMax, double samplingFreq, short[] samples)
    {
        super();
        this.channelNr = channelNr;
        this.bits = bits;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.sensitivity = (rangeMax-rangeMin)/(Math.pow(2,bits)-1);
        this.samplingFreq = samplingFreq;
        this.samples = samples;
        this.channelName = "DNE";   // does not exist
        this.unit = "U";            // unspecified
        this.length = this.samples.length;
    }

    public double value(int i)
    {
        if(i >= 0 && i < this.length)
        {
            return this.sensitivity*this.samples[i]+this.rangeMin;
        }
        else if(i >= -this.length && i < 0)
        {
            return this.sensitivity*this.samples[this.length+i]+this.rangeMin;
        }
        else
        {
            return Double.NaN;
        }
    }

    public double mean()
    {
        double result = 0.0;
        for(int i = 0; i < this.length; i++)
        {
            result += this.value(i);
        }
        return result/this.length;
    }

    public double rms()
    {
        double result = 0.0;
        for(int i = 0; i < this.length; i++)
        {
            result += Math.pow(this.value(i),2);
        }
        return Math.pow(result/this.length,0.5);
    }

    public String toString()
    {
        String result = "Channel name:\t" + this.channelName + "\nChanner ID:\t\t" + this.channelNr + "\nBits:\t\t\t" + this.bits +
                "\nSamples:\t\t" + this.length + "\nUnit:\t\t\t" + this.unit + "\nSensitivity:\t" + this.sensitivity + " " + this.unit +
                "\nMin read:\t\t" + this.rangeMin + "\nMax read:\t\t" + this.rangeMax + "\nSampling Freq:\t" + this.samplingFreq +
                " Hz\nMean:\t\t\t" + this.mean() + " " + this.unit + "\nRMS:\t\t\t" + this.rms() + " " + this.unit + "\nValues [" + this.unit + "]:";

        for(int i = 0; i < this.length; i++)
        {
            result += "\n" + i + ".\t" + this.value(i);
        }

        return result;
    }
}